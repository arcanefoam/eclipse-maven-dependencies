# Eclipse Maven Dependencies

This project is a skeleton to create dependencies plugin for other Eclipse plugins.

## Instructions
1. Update the MANIFEST.MF with your project information
2. Update the maven pom with your project information.
3. Add any dependencies as usual. Note that since this is not used for an actual maven project, adding dependencies only for *test* does not have any impact.
4. Run the `UpdateClasspathWithMavenDeps` script. This scripts copies the library jars to the lib folder and creates the *scripts/jarEntries.tmp* file. E.g on Unix
```
    arcanefoam$ sh UpdateClasspathWithMavenDeps.sh
```
5. Open the Navigator view: Window->Show View->Navigator
6. Navigate to your project and open the *.classpath* file
7. Copy the contents of the generated *scripts/jarEntries.tmp* file to the top of your classpath file
8.  
