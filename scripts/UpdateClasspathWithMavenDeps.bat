@echo off
REM This script uses maven to download the dependencies and then copies them to the lib folder
REM It also generates a jarEntries.tmp file that provides the classpath entries that need to be added
REM to the project's .classpath file.

cd ..
rm -rf lib/

REM Use latest versions
REM mvn versions:display-property-updates -Dversions.allowSnapshots=true -DallowMinorUpdates=false -DallowMajorUpdates=false -DallowIncrementalUpdates=false
REM mvn versions:update-properties -Dversions.allowSnapshots=true -DallowMinorUpdates=false -DallowMajorUpdates=false -DallowIncrementalUpdates=false

call mvn clean install

REM Add the new dependencies to the classpath. Crate then entry list in a separate file since modifying the .classpath file breakes eclipse
rm -f lib/jarEntries.tmp
SetLocal EnableDelayedExpansion
for /r lib/ %%f in (*.jar) do (
    set name=%%~nf
    set fullname=%%~dpnf
    echo.!name! | findstr /C:"source" 1>nul
    if errorlevel 1 (
        REM echo. Not a source file. Looking for source
        if exist !fullname!-sources.jar (
            rem file exists
            echo     ^<classpathentry exported="true" kind="lib" path="lib/!name!.jar" sourcepath="lib/!name!-sources.jar"/^> >> scripts/jarEntries.tmp
        ) else (
            rem file doesn't exist
            echo     ^<classpathentry exported="true" kind="lib" path="lib/!name!.jar"/^> >> lib/jarEntries.tmp
        )
      
    )
    REM 
    )

exit /b